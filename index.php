<?php
/*
This is the unique entrypoint. It is called at every request.
All requests MUST be in POST to avoid sniffing.
The parameters are as follows:
key - the file name
action - "read" or "write" or "delete"
file (optional) - the actual file data (like in a normal upload)

NB: store the files in the "files" folder
*/

include("security.php");

if ((isset($_POST['key'])) && (isset($_POST['secret']))) {
    //this is a POST request, let us check security
    if ((isAuthIP()) && (isCorrectSecret())) {
        //security is okay, let us do the work
        switch($_POST['key']) {
            case "read":
                //1-check if the file exists
                //2-if the file does not exist, send a failure message
                //3-if file exist, use PHP to treat this request as a download
            break;
            //continue for the other two cases (write and delete)
            //NB: Both "write" and "delete" only respond with {status:"success"} or {status:"failed"}
        }
    }
}