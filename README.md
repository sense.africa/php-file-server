This is a simple PHP-based remote file server.
It allows read, write and delete.
Access is controlled by password string and IP-check.